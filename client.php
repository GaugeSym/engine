<html>

 <head>
  <title>TITLE</title>
  <LINK href="style.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/png" href="../icon.png"/>
 </head>


 <body>

  <div id="div_nameselect">
   <img id="img_lanFlag" src="img/uk.png" width="20" height="20" onclick="toggleLanguage()">
   <b>Name: </b>
   <input type="text" id="input_name" value=""/>
   <button id="button_name" onClick="selectName()">OK</button>
  <div/>

  <script src="includeHTML/includeHTML.js"></script>
  <div data-src="../client.html"></div>

  <div id="div_vote">
   <br>
   <input type="text" class="no-outline" id="txt_answer" value="" size="100" readonly>
   <br>
   <button id="button_yes" class="button buttonGreen" onclick="voteYes()">yes</button>
   <button id="button_no" class="button buttonGreen" onclick="voteNo()">no</button>
  <div/>

  <div id="div_score"><div/>

 </body>

</html>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="../client.js"></script>

<script type="text/javascript">

 var gameState = new Map();

 var lang = 'EN_'

 const translate = {
  'DE_score' : 'PunkteStand',
  'EN_score' : 'Score'
 }

 function toggleLanguage(){
  flag = document.getElementById("img_lanFlag");

  if(lang == 'DE_'){
   flag.src = "img/uk.png";
   lang = 'EN_';
  }

  else if(lang == 'EN_'){
   flag.src = "img/de.png";
   lang = 'DE_';
  }

  communicate('changeLang ' + lang)

 }

 var lobby = "<?php echo $_GET['lobby']; ?>";
 var client = "<?php echo $_GET['client']; ?>";

 var gameState = new Map();

 function randString(length){

  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;

  for(var i = 0; i < length; i++ ){
   result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;

 }

 if(lobby.length < 1){
  lobby = randString(8);
  window.location.href = 'client.php?lobby='+lobby;
 }

 if(client.length < 1){
  client = randString(8);
  name = 'NAME';
  communicate('addClient ' + name);
  window.location.href = 'client.php?lobby='+lobby+'&client='+client;
 }

 function selectName(){
  nameInput = document.getElementById("input_name").value;
  communicate('setName ' + nameInput.replace(' ', '_'));


  initInsert();

 }

 function voteYes(){
  communicate('button yes');
 }

 function voteNo(){
  communicate('button no');
 }

 function communicate(message){
  console.log('communicate: ' + message);
  if(message == 'listen') port = 9997;
  else port = 9998;
  return $.ajax({
   url:"communicate.php?message="+message+"&lobby="+lobby+"&client="+client+"&port="+port,
   success : function(result){
    try{
     gameState = JSON.parse(result);
     updatePage();
    }
    catch(e){
     console.log('WARNING in communicate(): invalid json string');
    }
   },
   error : function(xhr, textStatus, errorThrown){
    console.log('WARNING in communicate(): retry after status ' + xhr.status);
    $.ajax(this); //try again
    return;
   }
  });
 }

 function updatePage(){

  //link
  let link =  window.location.toString();
  let endPos = link.indexOf('&client');
  link = link.substr(0, endPos);

  //score
  scoreTxt = '<b>Link:</b><br>' + link + '<br><br>';
  scoreTxt += '<b>' + translate[lang + 'score'] + ':</b><br/>';
  for (var clientKey in gameState['clients']){
   if(gameState['clients'][clientKey]['name'] == 'NAME') continue;
   scoreTxt += gameState['clients'][clientKey]['name'].replace('_', ' ') + ': ' + gameState['clients'][clientKey]['score'];
   scoreTxt += '<br/>'
  }
  document.getElementById("div_score").innerHTML = scoreTxt;

  //answer
  if(gameState['clientUnderVote'] != ''){
   var answerText = '';
   answerText = gameState['clients'][gameState['clientUnderVote']]['name'].replace('_', ' ');
   answerText += " answers:";
   answerText += " '" + gameState['answerUnderVote'] + "' ";
   answerText += '(' + (gameState['nVotesYes'] + gameState['nVotesNo']) + '/' + (Object.keys(gameState['clients']).length - 1) + ' ' + 'votes' + ')';
   document.getElementById("txt_answer").value = answerText;
  }
  else{
   document.getElementById("txt_answer").value = gameState['answerText'];
  }

  //button yes and no
  if(gameState['clientUnderVote'] == '' || gameState['clientUnderVote'] == client){
   document.getElementById("button_yes").disabled = true;
   document.getElementById("button_no").disabled = true;
  }
  else{
   document.getElementById("button_yes").disabled = false;
   document.getElementById("button_no").disabled = false;
  }
  if(gameState['clients'][client]['button_yes'] == true){
   document.getElementById("button_yes").style = 'border-style:dotted';
  }
  else{
   document.getElementById("button_yes").style = 'border-style:solid';
  }
  if(gameState['clients'][client]['button_no'] == true){
   document.getElementById("button_no").style = 'border-style:dotted';
  }
  else{
   document.getElementById("button_no").style = 'border-style:solid';
  }
  if(document.getElementById("txt_answer").value == ''){
   document.getElementById("button_yes").disabled = true;
   document.getElementById("button_no").disabled = true;
  }

  updatePageInsert();

 }

 async function listenToBroadcast() {
  while(true) {
   console.log('TEST');
   try{
    result = await communicate('listen');
    console.log(result);
   }
   catch(e){
    console.log('WARNING in listenToBroadcast: await failed');
   }
   console.log('DONE');
  }
 }

 listenToBroadcast();

</script>
