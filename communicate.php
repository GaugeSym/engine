<?php

$port = $_GET['port'];
$address = gethostbyname('localhost');

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

$result = socket_connect($socket, $address, $port);
if ($result === false) {
    echo "socket_connect failed\nreason: " . socket_strerror(socket_last_error($socket)) . "\n";
}

$lobby = $_GET['lobby'];
$client = $_GET['client'];
$in = "$lobby $client ";
$in .= $_GET['message'];
$in .= "\r\n";
$out = '';

socket_write($socket, $in, strlen($in));

while ($out = socket_read($socket, 2048)) {
    echo $out;
}

socket_close($socket);

?>
