import socketserver
import threading
import random
import time
import json
from serverInserts import initLobbyInsert, initClientInsert, handleInsert, evaluateYesNoVoteInsert

debug = True

gameState = dict()
mutex = dict()

def getTime():

 return round(time.time() * 1000)

def initLobby():

 return {
  'clients': dict(),
  'staleClients': [],
  'lastPing': getTime(),
  'nVotesNewGame': 0,
  'nVotesYes': 0,
  'nVotesNo': 0,
  'clientUnderVote': '',
  'answerUnderVote': '',
  'answerText': '',
 }

def initClient():

 return {
  'name': 'NAME',
  'buttonNewGame': False,
  'buttonYes': False,
  'buttonNo': False,
  'score': 0,
  'lastPing': getTime(),
 }

def newGame(keyLobby):

 for keyClient in gameState[keyLobby]['clients']:
  gameState[keyLobby]['clients'][keyClient]['buttonNewGame'] = False
  gameState[keyLobby]['clients'][keyClient]['score'] = 0


def evaluateYesNoVote(keyLobby):

 gameState[keyLobby]['voteOutcome'] = 'Ergebnis: ja: ' + str(gameState[keyLobby]['nVotesYes']) + ' nein: ' + str(gameState[keyLobby]['nVotesNo']) + ' > '

 evaluateYesNoVoteInsert(gameState[keyLobby])

 gameState[keyLobby]['clientUnderVote'] = ''

 for dicKey in gameState[keyLobby]['clients']:
  gameState[keyLobby]['clients'][dicKey]['buttonYes'] = False
  gameState[keyLobby]['clients'][dicKey]['buttonNo'] = False

class server_broadcast(socketserver.BaseRequestHandler):

 def handle(self):

  global gameState
  global mutex

  self.data = self.request.recv(2048).strip()
  message = self.data.decode('utf-8')

  if debug: print('>>', message)

  [keyLobby, keyClient, messageType] = message.split(' ')

  if not keyLobby in mutex:
   message = json.dumps({}).encode()
   self.request.sendall(message)
   if debug: print('<< (WARN1)', message)
   return None

  if not keyClient in mutex[keyLobby]:
   message = json.dumps({}).encode()
   self.request.sendall(message)
   if debug: print('<< (WARN2)', message)
   return None

  mutex[keyLobby][keyClient].acquire()

  now = getTime()
  gameState[keyLobby]['lastPing'] = now
  gameState[keyLobby]['clients'][keyClient]['lastPing'] = now

  if keyLobby in gameState:
   message = json.dumps(gameState[keyLobby]).encode()
   self.request.sendall(message)
  else:
   message = json.dumps({}).encode()
   self.request.sendall(message)

  if debug: print('<<', message)

class server_dialog(socketserver.BaseRequestHandler):

 def handle(self):

  global gameState
  global mutex

  self.data = self.request.recv(2048).strip()
  message = self.data.decode('utf-8')
  if debug: print('>', message)

  [keyLobby, keyClient, messageType, messageContent] = message.split(' ')

  if not keyLobby in mutex: mutex[keyLobby] = dict()
  if not keyClient in mutex[keyLobby]: mutex[keyLobby][keyClient] = threading.Lock()

  if not keyLobby in gameState: gameState[keyLobby] = initLobbyInsert(initLobby())

  firstCommunication = False

  if not keyClient in gameState[keyLobby]['clients']:
   gameState[keyLobby]['clients'][keyClient] = initClientInsert(initClient())
   firstCommunication = True

  if messageType == 'setName':

   gameState[keyLobby]['clients'][keyClient]['name'] = messageContent

   for s, staleClient in enumerate(gameState[keyLobby]['staleClients']):

    if messageContent == staleClient['name']:

     for attribute in ['score']: #TODO

      gameState[keyLobby]['clients'][keyClient][attribute] = staleClient[attribute]

     del gameState[keyLobby]['staleClients'][s]

     break

  elif messageType == 'button':

   if messageContent == 'newGame': gameState[keyLobby]['clients'][keyClient]['buttonNewGame'] ^= True
   if messageContent == 'yes': gameState[keyLobby]['clients'][keyClient]['buttonYes'] ^= True
   if messageContent == 'no': gameState[keyLobby]['clients'][keyClient]['buttonNo'] ^= True

  nClients = 0
  gameState[keyLobby]['nVotesNewGame'] = 0
  gameState[keyLobby]['nVotesYes'] = 0
  gameState[keyLobby]['nVotesNo'] = 0

  for keyClient in gameState[keyLobby]['clients']:

   if gameState[keyLobby]['clients'][keyClient]['buttonNewGame']: gameState[keyLobby]['nVotesNewGame'] += 1
   if gameState[keyLobby]['clients'][keyClient]['buttonYes']: gameState[keyLobby]['nVotesYes'] += 1
   if gameState[keyLobby]['clients'][keyClient]['buttonNo']: gameState[keyLobby]['nVotesNo'] += 1
   if not gameState[keyLobby]['clients'][keyClient]['name'] == 'NAME': nClients += 1

  if gameState[keyLobby]['nVotesNewGame'] == nClients: newGame(keyLobby)
  if gameState[keyLobby]['nVotesYes'] + gameState[keyLobby]['nVotesNo'] == nClients - 1 and nClients > 1: evaluateYesNoVote(keyLobby)

  gameState[keyLobby] = handleInsert(gameState[keyLobby], message)

  now = getTime()
  gameState[keyLobby]['lastPing'] = now
  gameState[keyLobby]['clients'][keyClient]['lastPing'] = now

  gameState[keyLobby]['message'] = message

  if firstCommunication:
   message = json.dumps(gameState[keyLobby]).encode()
   self.request.sendall(message)
   if debug: print('<', message)

  for keyLobby in gameState:
   for keyClient in gameState[keyLobby]['clients']:
    if mutex[keyLobby][keyClient].locked(): mutex[keyLobby][keyClient].release()

class myThread (threading.Thread):
   def __init__(self, threadID, name):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
   def run(self):
      print("Starting " + self.name + " server")
      if self.name == 'broadcast':
       try:
        serve_broadcast = socketserver.TCPServer(('localhost', 9997), server_broadcast)
        serve_broadcast.serve_forever()
       except ValueError:
        exit()
      elif self.name == 'dialog':
       try:
        serve_dialog = socketserver.TCPServer(('localhost', 9998), server_dialog)
        serve_dialog.serve_forever()
       except ValueError:
        exit()
      print("Exiting " + self.name)

if __name__ == "__main__":

 # Create new threads
 thread1 = myThread(1, "dialog")
 thread2 = myThread(2, "broadcast")

 # Start new Threads
 thread1.start()
 thread2.start()
